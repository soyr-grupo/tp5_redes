ifconfig eth0 220.189.234.35/27
ip route add default via 220.189.234.33
ip route add 220.189.232.0/24 via 220.189.234.33
ip route add 220.189.234.64/26 via 220.189.234.33
ip route add 220.189.234.192/27 via 220.189.234.33
