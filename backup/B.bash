ifconfig eth0 220.189.234.242/30
ifconfig eth1 220.189.234.250/30
ifconfig eth2 220.189.234.193/27
ip route add default via 220.189.234.241
ip route add 220.189.232.0/24 via 220.189.234.249
ip route add 220.189.234.32/27 via 220.189.234.241
ip route add 220.189.234.128/26 via 220.189.234.241
